difference(){
  difference(){
    union(){
      rotate(a = 90, v = [0, 1, 0]){
        cylinder(center = true, d = 40, h = 35);
      }
      translate(v = [0, 0, 77]){
        cube(center = true, size = [35, 40, 150]);
      }
    }
    rotate(a = 90, v = [0, 1, 0]){
      cylinder(center = true, d = 12, h = 36);
    }
  }
  translate(v = [0, 5, 155]){
    rotate(a = 90, v = [0, 1, 0]){
      cylinder(center = true, d = 35, h = 36);
    }
  }
}
