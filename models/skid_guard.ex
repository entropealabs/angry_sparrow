defmodule SkidGuard do
  use OpenSCAD

  def main() do
    outer =
      cylinder(h: 35, d: 40, center: true)
      |> rotate(a: 90, v: [0, 1, 0])

    inner =
      cylinder(h: 36, d: 12, center: true)
      |> rotate(a: 90, v: [0, 1, 0])

    guard =
      cube(size: [35, 40, 150], center: true)
      |> translate(v: [0, 0, 77])

    flush =
      cylinder(h: 36, d: 35, center: true)
      |> rotate(a: 90, v: [0, 1, 0])
      |> translate(v: [0, 5, 155])

    mount = union([outer, guard])

    mount
    |> difference(inner)
    |> difference(flush)
    |> write("skid_guard.scad")
  end
end
