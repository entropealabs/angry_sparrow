defmodule AngrySparrow do
  @moduledoc """
  Documentation for `AngrySparrow`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> AngrySparrow.hello()
      :world

  """
  def hello do
    :world
  end
end
