# Furitek Angry Sparrow

Parts for the Furitek Angry Sparrow 1/24 crawler frame. Original code is written using the [OpenSCAD](https://hexdocs.pm/open_scad/api-reference.html) library for Elixir.

I ran into issues using more recent versions of Elixir/Erlang, so I have a branch here that fixes those. [https://github.com/entone/open_scad/](https://github.com/entone/open_scad/)

When importing the STLs into your preferred slicer, you will need to scale them to 10%. The gcode files should be good to go.

## Parts

* skid guards
* _electronics tray (coming soon)_

### Skid Guards

I found the carbon fiber frame would get caught on sharp edges and narrow spots, this helps it slide a bit better and keep narrow edges from catching.

![Skid Guards Installed](./images/skid_guards_installed.jpg)
![Skid Guards Cura](./images/skid_guards_cura.png)
![Skid Guards OpenSCAD](./images/skid_guards_open_scad.png)

gcode is configured for PLA.


